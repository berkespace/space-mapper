﻿using Space.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Space
{
    /// <summary>
    /// MainWindow.xaml etkileşim mantığı
    /// </summary>
    public partial class MainWindow
    {
        readonly RestaurantDB context = new RestaurantDB();
        AdminPage Ap = new AdminPage();
        public MainWindow()
        {
            InitializeComponent();
        }

    
        private void Login_Click(object sender, RoutedEventArgs e)
        {
          
            var login = context.Accounts.FirstOrDefault(s => s.Password == PassWord.Text);
            if (login == null)
            {

                MessageBox.Show("Yanlış şifre.");
                return;
            }

            var authority = login.Authority;
            if (authority == "2")
            {
                this.Close();
                Ap.Show();
                
            }



        }
    }
}
