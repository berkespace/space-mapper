namespace Space.Database
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Net.Security;
    using System.Windows.Markup;

    public partial class Accounts
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Surname { get; set; }

        public string Password { get; set; }

        public DateTime? Joindate { get; set; }

        [StringLength(50)]
        public string Authority { get; set; }

        public bool? Status { get; set; }

        public override string ToString()
        {
            
            return Authority;
        }

        
    }

}
