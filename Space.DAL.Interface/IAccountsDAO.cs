﻿using Space.Data.Enums;
using Space.Data;
using System;

namespace Space.DAL.Interface
{
    public interface IAccountsDAO
    {
        #region Methods 
        DeleteEnums Delete(long accountId);
        SaveEnums InsertOrUpdate(ref AccountsDTO account);
        AccountsDTO LoadById(long accountId);
        AccountsDTO LoadByName(string name);
        void WriteGeneralLog(long accountId, string name,  string logData);
        #endregion

    }
}
