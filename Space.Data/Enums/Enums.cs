﻿using System;

namespace Space.Data.Enums
{
    public enum DeleteEnums : byte
    {
        Unknown = 0,
        Deleted = 1,
        Error = 2,
        NotFound = 3
    }
    public enum SaveEnums : byte
    {
        Unknown = 0,
        Inserted = 1,
        Updated = 2,
        Error = 3
    }
}
