﻿using System;
using Space.DAL.Interface;
using Space.Data.Enums;
using Space;
using Space.Helpers;
using Space.Database;
using System.Linq;
using Space.Data;
using System.Data.Entity;
using Space.Mapper;

namespace Space.DAL.DAO
{
    public class AccountDAO : IAccountsDAO
    {
        private static AccountsDTO insert(AccountsDTO account, RestaurantDB context)
        {
            Accounts entity = new Accounts();
           AccountMapper.ToAccounts(account, entity);
            context.Accounts.Add(entity);
            context.SaveChanges();
            AccountMapper.ToAccountsDTO(entity, account);
            return account;
        }

        public AccountsDTO LoadById(long accountId)
        {
            try
            {
                using (RestaurantDB context = DataAccessHelper.CreateContext())
                {
                    Accounts account = context.Accounts.FirstOrDefault(a => a.Id.Equals(accountId));
                    if (account != null)
                    {
                        AccountsDTO accountDTO = new AccountsDTO();
                        if (AccountMapper.ToAccountsDTO(account, accountDTO))
                        {
                            return accountDTO;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return null;
        }
        private static AccountsDTO update(Accounts entity, AccountsDTO account, RestaurantDB context)
        {
            if (entity != null)
            {
               AccountMapper.ToAccounts(account, entity);
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
            }
            if (AccountMapper.ToAccountsDTO(entity, account))
            {
                return account;
            }

            return null;
        }
        public AccountsDTO LoadByName(string name)
        {
            try
            {
                using (RestaurantDB context = DataAccessHelper.CreateContext())
                {
                    Accounts account = context.Accounts.FirstOrDefault(a => a.Name.Equals(name));
                    if (account != null)
                    {
                        AccountsDTO accountDTO = new AccountsDTO();
                        if (AccountMapper.ToAccountsDTO(account, accountDTO))
                        {
                            return accountDTO;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return null;
        }
        public void WriteGeneralLog(long accountId, string name, string logData)
        {
            try
            {
                using (RestaurantDB context = DataAccessHelper.CreateContext())
                {
                    AccountLogs log = new AccountLogs
                    {
                        Id= (int)accountId,
                        Name=name,
                        Date=DateTime.Now,
                        Log=logData
                    
                    };

                    context.AccountLogs.Add(log);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public SaveEnums InsertOrUpdate(ref AccountsDTO account)
        {
            try
            {
                using (RestaurantDB context = DataAccessHelper.CreateContext())
                {
                    long accountId = account.Id;
                    Accounts entity = context.Accounts.FirstOrDefault(c =>c.Id.Equals(accountId));

                    if (entity == null)
                    {
                        account = insert(account, context);
                        return SaveEnums.Inserted;
                    }
                    account = update(entity, account, context);
                    return SaveEnums.Updated;
                }
            }
            catch (Exception e)
            {
              
                return SaveEnums.Error;
            }
        }

        public DeleteEnums Delete(long accountId)
        {
            try
            {
                using (RestaurantDB context = DataAccessHelper.CreateContext())
                {
                    Accounts account = context.Accounts.FirstOrDefault(c => c.Id.Equals(accountId));

                    if (account != null)
                    {
                        context.Accounts.Remove(account);
                        context.SaveChanges();
                    }

                    return DeleteEnums.Deleted;
                }
            }
            catch (Exception e)
            {
                return DeleteEnums.Error;
            }
        }

    }

  
}
