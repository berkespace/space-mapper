﻿using Space.Data;
using Space.Database;
using System;

namespace Space.Mapper
{
    public static class AccountMapper
    {
        public static bool ToAccounts(AccountsDTO input, Accounts output)
        {
            if (input == null)
            {
                return false;
            }

            output.Id = input.Id;
            output.Name = input.Name;
            output.Surname = input.Surname;
            output.Password = input.Password;
            output.Joindate = input.Joindate;
            output.Authority = input.Authority;
            output.Status = input.Status;

            return true;
        }

        public static bool ToAccountsDTO(Accounts input, AccountsDTO output)
        {
            if (input == null)
            {
                return false;
            }

            output.Id = input.Id;
            output.Name = input.Name;
            output.Surname = input.Surname;
            output.Password = input.Password;
            output.Joindate = input.Joindate;
            output.Authority = input.Authority;
            output.Status = input.Status;

            return true;
        }
    }
}
